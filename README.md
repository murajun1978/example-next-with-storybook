# Example of Next.js with Storybook

## Get Started

Clone the repository:

```
$ git clone https://gitlab.com:murajun1978/example-next-with-storybook.git
$ cd example-next-with-storybook
```

Install dependencies:

```
$ npm install
```

Run Next.js with Dev mode:

```
$ npm run dev
```

Run Storybook:

```
$ npm run storybook
```
